const { response } = require('express');
const bcrypt = require('bcryptjs');

const Usuario = require('../models/usuarios');
const { generarJWT } = require('../helpers/jwt');

// CONTROLADORES

// creamos el usuario
const crearUsuario = async (req, res = response) => {
   console.log('Creando Usuario');
   
   // NOTA: 'res' es la respuest que se va tener de la peticion POST

   // extraer informacion que me insteresa de la req.body
   const { email, password } = req.body;

   // verificar que no exista en la base de datos porque tengo configurado que este valor no debe ser igual a otro guardado
   // lo hacemos en un try catch, si hay problemas con la base de datos no me reviente la aplicacion
   try { 

      // buscamos el email a ver si existe o esta repetido
      const existeEmail = await Usuario.findOne({ email });
      if( existeEmail ) { // si existe enviamos msg erro 400 
         return res.status(400).json({
            ok: false,
            msg: 'El email ya existe'
         });
      }

      // creamos instancia del modelo de la autorizacion del usuario
      const usuario = new Usuario( req.body );

      // encripatr la contraserna
      const salt = bcrypt.genSaltSync(); // generamos un salt
      usuario.password = bcrypt.hashSync(password, salt ); // mandamos el password y como argumento el salt
 
      // enviamos a la base de datos los datos del ususario
      await usuario.save();

      // generar jsonwebtoken (JWT) para que el usuario pueda acceder de manera segura
      // le mandamos el id del usuario para crear el JsonWebToken
      // el valor del token lo puedes meter en https://jwt.io/ para ver los valores
      const token = await generarJWT(usuario.id); // me importo '../helpers/jwt'

      res.json({
         ok: true,
         // body: req.body // me devuelve lo que estoy ingresando en el json del header en el POST
         usuario, // me devuelve el usuario creado en la base de datos
         token // devolvemoms el token
      });

      /* en caso que se guarde se va recibir la respuesta BRUTA

         {
            "ok": true,
            "usuario": {
               "nombre": "Jeancarlos",
               "email": "test4@test.com",
               "password": "12345",
               "online": false,
               "_id": "62e2b0c8cfa970fd31aceb69",
               "__v": 0
            }
         }

      */



   } catch (error) {
      console.log(error);
      res.status(500).json({
         ok: false,
         msg: 'Error al conectar con el Servidor'
      });
      
   }
}

// login sesion del usuario
const login = async (req, res = response) => {
   console.log('Iniciando Sesion');
   
   
   // extraer informacion que me insteresa de la req.body
   const { email, password } = req.body;

   try {

      // Validar email. buscamos el email si no se escuenta significa que no esta registrado el usuario
      const usuarioDB = await Usuario.findOne({ email });
      if(!usuarioDB) {
         return res.status(404).json({
            ok: false,
            msg: 'Email no encontrado'
         });
      }

      // validar el Password, si paso la validacion de arriba me trae todo el modelo del usuario con su pasword
      const validPassword = bcrypt.compareSync( password, usuarioDB.password );
      if(!validPassword) {
         return res.status(400).json({
            ok: false,
            msg: 'Contrasena no valido'
         });
      }

      // si llego aqui es que el email y el password estan bien
      // Generamos el JWT de nuevo
      const token = await generarJWT( usuarioDB.id );

      // mandamos la respuesta de la peticion POST
      res.json({
         ok: true,
         // body: req.body // me devuelve lo que estoy ingresando en el json del header en el POST
         usuario: usuarioDB, // me devuelve el usuario creado en la base de datos
         token // devolvemoms el token
      });

   } catch (error) {
      console.log(error);
      res.status(500).json({
         ok: false,
         msg: 'Error al conectar con el Servidor'
      });
   }
}

// renovar el JWT.
const renewToken = async (req, res = response) => {
   console.log('Renovando JWT');

   // console.log(req.uid);
    
   // const uid uid del usuario
   // extraer informacion que me insteresa de la req.body
   const uid = req.uid;

   // console.log(uid); 

   try {
      //generar nuevo JWT 
      // generar jsonwebtoken (JWT) para que el usuario pueda acceder de manera segura
      // le mandamos el id del usuario para crear el JsonWebToken
      // el valor del token lo puedes meter en https://jwt.io/ para ver los valores
      const token = await generarJWT( uid ); // me importo '../helpers/jwt'

      console.log('Token nuevo: ', token);
      
      // obtner el ususario por uid, Usuario.findById...
      // Buscado el usuarui segun el ID
      const usuario = await Usuario.findById( uid );
      if(!usuario) {
         return res.status(404).json({
            ok: false,
            msg: 'Id del usuario NO valido'
         });
      }

      // me va generar el nuevo token y se debe guardar en el dispositivo para ser usando en la proxima sesion
      res.json({
         ok: true,
         usuario: usuario,
         token
      });
 
   } catch (error) {
      console.log(error);
      res.status(500).json({
         ok: false,
         msg: 'Error al conectar con el Servidor'
      });
   }
 
}

module.exports = { // lo neto entre llaves para que se por nombre
   crearUsuario,
   login,
   renewToken,
}