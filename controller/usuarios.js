const { responce } = require('express');
const Usuario      = require('../models/usuarios');


const getUsuarios = async ( req, res = responce ) => { 
  console.log('Obteniendo Usuario Conectados'); 

  // hacer paginacion, si no existe desde entonces el valor por defecto es cero, sino arranca desde 'desde'
  const inicio = req.header('inicio'); 

  // const desde = Number( req.query.desde ) || 0;
  const desde = inicio || 0;

  // busca en la base de datos todos los usuarios los ordena online si le colocas de menos - los ordena conectados primero
  // adentro del find esta un condicional de la busqueda
  // el $ne significa que no contenga o no exista ese elemento uid lo capture en la validacion del JWT
  // skip() significa que salte todos los anteriores y mmuestre los siguientes
  try {  
    const usuarios = await Usuario
      .find({ _id: { $ne: req.uid } })
      .sort('-online')
      .skip(desde)
      .limit(20);

    res.json({
        ok: true,
        usuarios
    });

  } catch (error) {
    console.log(error);
    res.status(500).json({
        ok: false,
        msg: 'Error al conectar con el Servidor'
    });
  } 
}

module.exports = {
  getUsuarios
}


  /* Respuesta bruta de la base de datos

      {
        "ok": true,
        "usuario": {
            "nombre": "Jeancarlos",
            "email": "test4@test.com",
            "password": "12345",
            "online": false,
            "_id": "62e2b0c8cfa970fd31aceb69",
            "__v": 0
        }
      }

  */