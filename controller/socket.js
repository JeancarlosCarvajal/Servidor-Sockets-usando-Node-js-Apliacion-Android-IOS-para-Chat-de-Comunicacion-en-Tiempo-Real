// importammos en modelo de usuario
const Mensaje = require('../models/mensaje');
const Usuario = require('../models/usuarios'); 



// metodos de interaccion con los sockets

// cuando el usuario se conecta para actualizar la base de datos
const usuarioConectado = async ( uid = '' ) => { 
  // buscamos en la base de datos mongoose
  const usuario  = await Usuario.findById( uid ); 
  // seteamos el modelo del usuario en true online
  usuario.online = true;
  // Grabar el nuevo modelo del usuario en la base de datos
  await usuario.save(); 
  // retornamos el usuario 
  return usuario;
}

// cuando el usuario se Desconecta para actualizar la base de datos
const usuarioDesconectado = async ( uid = '' ) => { 
  // buscamos en la base de datos mongoose
  const usuario  = await Usuario.findById( uid ); 
  // seteamos el modelo del usuario en true online
  usuario.online = false;
  // Grabar el nuevo modelo del usuario en la base de datos
  await usuario.save(); 
  // retornamos el usuario 
  return usuario;
}

// grabar mensaje en la base de datos
const grabarMensaje = async ( payload ) => {
  /*
    payload: {
      de    : '',
      para  : '',
      texto : ''
    }
  
  */
  try {
    // creamos la instancia con el schema creado 
    const mensaje = new Mensaje(payload);
    // guardar en la base de datos. esperamos a que se guarde con el await
    await mensaje.save(); 
    // retornamos true en caso de exito
    return true;
  } catch (error) {
    console.log('Error al guardar el mensaje en la Base de Datos');
    return false;
  }
}

module.exports = {
  usuarioConectado,
  usuarioDesconectado,
  grabarMensaje
}