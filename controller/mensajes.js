const { responce } = require('express');
const Mensaje      = require('../models/mensaje');


const obtenerChat = async ( req, res = responce ) => { 
  console.log('Obteniendo Mensaje Conectados'); 

  // hacer paginacion, si no existe desde entonces el valor por defecto es cero, sino arranca desde 'desde'
  const inicio = req.header('inicio'); 

  // const desde = Number( req.query.desde ) || 0;
  const desde = inicio || 0;

  // ver el uid que se quier buscar
  const miId = req.uid;

  // ver los parametos ../memsajes/:de
  const mensajesDe = req.params.de; // .params.de

  // console.log('desde: ', desde);
  

  // busca en la base de datos todos los mensajes  
  // adentro del find esta un condicional de la busqueda
  // el $or significa que no contenga o no exista la condicion
  // skip() significa que salte todos los anteriores y mmuestre los siguientes
  try {  
    const mensajes = await Mensaje.find({ 
        $or: [{ de: miId, para: mensajesDe }, { de:  mensajesDe, para: miId }] 
      })
      .sort( {createdAt: 'asc'} )
      .skip(desde) // debe venir el los header de la solicitud, en caso que sean muchos se especica desde donde
      .limit(30);
      
    // tuve que traerme ascendente el arreglo par apoder ver el desde 
    // ahora vuelvo a ordenar el arreglo de forma descendente
    mensajes.sort( (a, b) => b.createdAt - a.createdAt );

    res.json({
        ok: true,
        miId: miId,
        mensajes
    });

  } catch (error) {
    console.log(error);
    res.status(500).json({
        ok: false,
        msg: 'Error al conectar con el Servidor'
    });
  } 
}

module.exports = {
  obtenerChat
}


  /* Respuesta bruta de la base de datos

      {
        "ok": true,
        "Mensaje": {
            "nombre": "Jeancarlos",
            "email": "test4@test.com",
            "password": "12345",
            "online": false,
            "_id": "62e2b0c8cfa970fd31aceb69",
            "__v": 0
        }
      }

  */