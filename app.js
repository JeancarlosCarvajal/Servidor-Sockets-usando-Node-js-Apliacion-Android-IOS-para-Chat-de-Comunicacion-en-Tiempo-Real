const express = require('express');
const { Module } = require('module');
const path = require('path');
require('dotenv').config(); 

// DB Config
const { dbConnection } = require('./database/config');
dbConnection();


// App de express
const app = express();


// Lectura y parceo del body. esto se llama "middleware" codigo que se ejecuta cuando va pasando por aqui
app.use( express.json() );


// Creamos el servidor de Sockets en Node
const server = require('https').createServer(app);
// exportamos el io al archivo de socket para poder exportarlo desde /sockets/sockets.js
module.exports.io = require('socket.io')(server);
// module.exports.io = require('socket.io')(server, {
//   cors: {
//     origin: "https://desarrollo-web3.com",
//     methods: ["GET", "POST"],
//   }
// });
// module.exports.io = require('socket.io')(server,{rejectUnauthorized: false}); // pruebas
// incuimos el archivo de socket que separamos para mayor orden en el proyecto
require('./sockets/socket.js');
  

// Path publico 
const publicPath = path.resolve(__dirname, 'public');
app.use(express.static(publicPath));



// Mis rutas, configurar el path .. esto se llama "middleware" codigo que se ejecuta cuando va pasando por aqui
// http://localhost:3000/api/login/new y los demas que estan en ese archivo
// el api/login proviene de aqui se crea requiriendo el auth
app.use( '/api/login', require('./routes/auth') );

// creamos la ruta para el metodo GET obtener usuarios conectados...
// http://localhost:3000/api/usuarios
app.use( '/api', require('./routes/usuarios') );

// creamos la ruta para el metodo GET obtener usuarios mensajes que me han llegado...
// http://localhost:3000/api/mensajes
app.use( '/api', require('./routes/mensajes') );

server.listen(process.env.PORT, ( err ) => { 
  if(err) throw new Error(err); 
  // server.__dirname
  // __dirname = /home/desarrolloweb3/nodejsapp 
  console.log('Iniciando Servidor en Puerto: ', process.env.PORT); 
});
 