const {io} = require('../app');
const { comprobarJWT } = require('../helpers/jwt');
const { usuarioConectado, usuarioDesconectado, grabarMensaje } = require('../controller/socket');




// Mensajes de Sockets. cliente es la computadores que se conecta al socket
io.on('connection', client =>  {
    console.log('Autenticando Cliente Socket (desde JS)');

    // cleinte con JWT para filtrar los que se conecten a nustro sockets
    // el cliente envio un extraHeaders
    console.log( client.handshake.headers );
    // console.log( client.handshake.headers['x-token'] ); 
    // console.log('token servidor: ', client.handshake.headers['x-token'] ); // no sirve en fluuerweb
    // console.log('Token Auth: ', client.handshake.auth['x-token'] ); // perfecto para web y mobil

    // en caso que contanga el header sino asigna el valor de auth al token 
    const token_auth = client.handshake.headers['x-token'] || client.handshake.auth['x-token'];
    console.log('Token Auth: ', token_auth ); // perfecto para web y mobil

    // usamos la funcion para verificar nuestro JWT creada dentro de helpers jwt.js
    const [valido, uid] = comprobarJWT( token_auth );
    // console.log(valido +' '+uid);

    // filtramos la conexion del cliente en caso que tenga un token valido
    // sino lo desconectamos
    if(!valido) return client.disconnect();


    // cliente autenticado
    console.log('Cliente Socket Conectado (desde JS)');
    usuarioConectado( uid );

    // Ingresar a una Sala en particular 
    // Sala Global de Chat, client.id, 62e466cf238daabf5d2c4fd7
    // crea una sala con el nombre del id del usuario 62e466cf238daabf5d2c4fd7

    // Aqui unimos el cliente a la sala 62e466cf238daabf5d2c4fd7, es decir se crea una sala con el uid del cliente que se acaba de conectar al socket
    // cada vez que alguien manda un mensaje con el uid del cliente entonces lo va porder escuhcar en el sockets ya que se ha creado una sala para exclusiva de el
    client.join( uid ); 
    // client.to( uid ).emit('evento'); // aqui le mandamos un mensaje a un cliente el particular en la sala 62e466cf238daabf5d2c4fd7
    
    // escuchamos el evento del mensaje personal
    client.on('mensaje-personal', async ( payload ) => {
        console.log(payload); 
        // console.log('Mensaje para uid: ', payload.para); 

        // Grabar mensajes. lo colocamos en un await para evitar seguir con el codigo sin que se guarde en la base de datos
        await grabarMensaje(payload);

        // ahora le mandajemos el mensaje al usuario al cual queremos
        // basicamente le estamos enviando todo lo que recibimos en el servidor al cliente como un espejo
        io.to( payload.para ).emit('mensaje-personal', payload);  
    });
    
    client.on('disconnect', () => {
        usuarioDesconectado( uid );
        console.log('Cliente Desconectado (desde JS)');
    });
 
    // // escuchar el mensaje desde el cliente
    // client.on('mensaje', payload => {
    //     console.log('Mensaje !!!', payload); // ojo payload ya no se usa
    //     io.emit('mensaje', {admin:'Nuevo mensaje'});
    // });
  
});