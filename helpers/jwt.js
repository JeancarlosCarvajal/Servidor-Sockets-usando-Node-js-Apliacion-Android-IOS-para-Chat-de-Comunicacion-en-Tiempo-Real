
// pagina oficial https://jwt.io/
// paquete Node https://www.npmjs.com/package/jsonwebtoken

// NOTA: Grabar informacion NOOO SENSIBLE OJOJOJOJOJ en los JWT
const jwt = require('jsonwebtoken');

const generarJWT = ( uid ) => {

   // creamos una promesa, y devolvemos el reject en caso que falle y resolve en caso que sea  todo bien
   return new Promise((resolve, reject) => {
      const payload = { uid };

      /* 
         el segundo argumento (process.env.JWT_KEY) es una identificacion unica del servidor
         si alguien llega a tener este valor puede firmar tokens como su fuese mi servidor
         documentacion https://jwt.io/ y https://www.npmjs.com/package/jsonwebtoken
      */
      jwt.sign( payload, process.env.JWT_KEY, {
         expiresIn: '24h'
      }, (err, token) => {
         
         // esto es la respuesta para el call back 
         if(err){
            // no se puedo crear el token
            reject('No se pudo realizar el JWT');
         }else{ // no hay error
            // TOKEN listo
            resolve(token);
         }
   
      });
   });

}

// validar el JWT que viene del Socket del cliente
const comprobarJWT = (token = '', next) => {
   // aqui validamos el JWT comparandolo con el JWT_KEY unico del servidor
   // console.log('Validando el token: ', token);
   
   try { 
      // verificamos con verify(), toma el token unico del servidor para validar el JWT del usuario
      const { uid } = jwt.verify( token, process.env.JWT_KEY );

      // si todo sale bien seguimos sino lo agarra el catch 
      // retornamos un arreglo con el uid
      return [true, uid];
      
   } catch (error) {
      console.log('No Autorizado Cliente Socket (desde JS)');

      return [false, null];
   } 
}


module.exports = {
   generarJWT,
   comprobarJWT
}