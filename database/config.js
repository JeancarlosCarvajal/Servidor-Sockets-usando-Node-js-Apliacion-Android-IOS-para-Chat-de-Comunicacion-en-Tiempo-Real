// importar paque te de mogoose
const mongoose = require('mongoose');


const dbConnection = async () => {
   try {
      // console.log('Init DB config');
      await mongoose.connect(process.env.DB_CNN);

      console.log('Conectado a Base Datos Online');
   } catch (error) {
      console.log(error);
      throw new Error('Error en la Base de Datos - Hable con el administrador');
   }
}

module.exports = {
   dbConnection
}
