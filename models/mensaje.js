

const { Schema, model } = require('mongoose');

// esto es para manejar las respuestas de la base de datos Mongo
// esto es propio de la base de datos
const MensajeSchema = Schema({

  de: {
    type: Schema.Types.ObjectId, // Hacer referencia a un objeto a la referencia a la coleccion de usuarios en la base de datos
    ref: 'Usuario', // hace referencia al modelo o la coleccion que vamos a apuntar
    required: true
  },

  para: {
    type: Schema.Types.ObjectId, // Hacer referencia a un objeto a la referencia a la coleccion de usuarios en la base de datos
    ref: 'Usuario',  // hace referencia al modelo o la coleccion que vamos a apuntar
    required: true
  },

  mensaje: {
    type: String,
    required: true
  }

}, {
  timestamps: true // opcion de Mongoose para manejar la fecha de la base de datos
});
// el timestamps: true me guarda con la fecha de ese momento

// extraer los valor que realmente necesito de la respuesta BRUTA
MensajeSchema.method('toJSON', function () {

  // extraemos lo que no queremos  __v, _id, password,
  const { __v, _id, ...object } = this.toObject();

  //retornamos el objeto formateado con lo datos que queremos en la forma indicada
  return object;

});

// exportamos el modelo lo renombramos como Usuario
module.exports = model('Mensaje', MensajeSchema);

/* en caso que se guarde se va recibir la respuesta BRUTA

  {
    "ok": true,
    "usuario": {
      "nombre": "Jeancarlos",
      "email": "test4@test.com",
      "password": "12345",
      "online": false,
      "_id": "62e2b0c8cfa970fd31aceb69",
      "__v": 0
    }
  }

*/
