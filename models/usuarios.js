

const { Schema, model } = require('mongoose');


const UsuarioSchema = Schema({
   
   
   nombre: {
      type: String,
      required: true
   },

   email: {
      type: String,
      required: true,
      unique: true
   },

   password: {
      type: String,
      required: true
   },

   online: {
      type: Boolean,
      default: false
   }


});

// extraer los valor que realmente necesito de la respuesta BRUTA
UsuarioSchema.method('toJSON', function () {

   // extraemos lo que no queremos  __v, _id, password,
   const { __v, _id, password, ...object } = this.toObject();
   
   // creamos un elemento dentro del objeto llamado uid y le asignamos el valor de _id
   object.uid = _id;

   //retornamos el objeto formateado con lo datos que queremos en la forma indicada
   return object;
   
});

// exportamos el modelo lo renombramos como Usuario
module.exports = model('Usuario', UsuarioSchema);

/* en caso que se guarde se va recibir la respuesta BRUTA

   {
      "ok": true,
      "usuario": {
         "nombre": "Jeancarlos",
         "email": "test4@test.com",
         "password": "12345",
         "online": false,
         "_id": "62e2b0c8cfa970fd31aceb69",
         "__v": 0
      }
   }
   
*/
