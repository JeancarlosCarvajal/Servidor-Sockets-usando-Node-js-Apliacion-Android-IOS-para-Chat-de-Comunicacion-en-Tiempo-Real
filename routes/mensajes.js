/*
   Path del servicio

   path: api/mensajes

*/

// esto es para interactuar con la base de datos usando express
const { Router } = require('express');  
const { validar_JWT } = require('../middleweres/validar-jwt');

const { obtenerChat } = require('../controller/mensajes');
const router = Router(); 

// Pedir los mensajes  el parametro :de sera leido 
router.get('/mensajes/:de', validar_JWT, obtenerChat);

// exportamos a router
module.exports = router;