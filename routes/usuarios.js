/*
   Path del servicio

   path: api/usuarios

*/

// esto es para interactuar con la base de datos usando express
const { Router } = require('express');  
const { getUsuarios } = require('../controller/usuarios');
const { validar_JWT } = require('../middleweres/validar-jwt');
const router = Router(); 

// Pedir los usuarios conectados onLine
router.get('/usuarios', validar_JWT, getUsuarios);


// exportamos a router
module.exports = router;