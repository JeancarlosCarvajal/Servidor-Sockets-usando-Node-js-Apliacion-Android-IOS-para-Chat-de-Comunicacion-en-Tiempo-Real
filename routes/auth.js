/*
   Path del servicio

   path: api/login

*/

// esto es para interactuar con la base de datos usando express
const { Router } = require('express'); 
const { check } = require('express-validator');

const { crearUsuario, login, renewToken } = require('../controller/auth');
const { validarCampos } = require('../middleweres/validar-campos');
const { validar_JWT } = require('../middleweres/validar-jwt');
const router = Router();

// res = responce para que VS code me ayude a predisponer
// http://localhost:3000/api/login/new
// el api/login proviene de index y el new de aqui
// creamos el motodo post para poder inter actuar con la base de datos
/* 
   al hacer post al enlace con PostMan me devolvera lo que esta dentro de re.json
   {
      ok: true,
      msg: 'Crear usuario'
   }
*/

// hay que agregar un middlewere entre new y crearUsuario para validar que se este enviando los datos correctos
// el check importo 'express-validator', se verifica campo por capo
// este valor me lo va enviar como error 'El nombre es obligatorio'
router.post('/new', [
   check('nombre', 'El nombre es obligatorio').not().isEmpty(),
   check('email', 'El email No es valido').isEmail(),
   check('password', 'El password debe ser minimo 5 caracteres').isLength({ min: 5 }),
   validarCampos // me importo '../middleweres/validar-campos'
], crearUsuario);

// metodo post para validar la sesion del usuario Login
router.post('/', [
   check('email', 'El email No es valido').isEmail(),
   check('password', 'El password debe ser minimo 5 caracteres').isLength({ min: 5 }),
   validarCampos // me importo '../middleweres/validar-campos'
], login);

// renovar el JWT. usamos el middlewere validar_JWT para validarlo
router.get('/renew', validar_JWT, renewToken);

// exportamos a router
module.exports = router;