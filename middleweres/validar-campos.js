const { validationResult } = require('express-validator');



// me traje este validar campos para ordenar el codigo
// el next es para que se ejecute el proximo middlewere en caso que pase la prueba sino se quede parado en ese punto
const validarCampos = (req, res, next) => {
 
   // validationResult me importo 'express-validator'
   const errores = validationResult(req); 
   // console.log(errores);
   // si hay errores entonces responde con un bad request
   if(!errores.isEmpty()){
      return res.status(400).json({
         ok: false,
         errors: errores.mapped()
      });
   }

   // en caso que paso la validacion anterior entonces prosiga con el siguiente next 
   next();
}

module.exports = {
   validarCampos
}