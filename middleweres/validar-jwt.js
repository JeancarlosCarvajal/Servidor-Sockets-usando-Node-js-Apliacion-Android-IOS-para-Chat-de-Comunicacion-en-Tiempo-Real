const jwt = require('jsonwebtoken');



const validar_JWT = (req, res, next) => {

   // leer el token
   const token = req.header('x-token'); 
   // console.log('token viejo: ', token);

   // verificamos si hay token en la peticion
   if(!token){
      return res.status(401).json({
         ok: false,
         msg: 'No hay token en la Peticion'
      });
   }

   // aqui validamos el JWT comparandolo con el JWT_KEY unico del servidor
   try { 
      // verificamos con verify(), toma el token unico del servidor para validar el JWT del usuario
      const { uid } = jwt.verify( token, process.env.JWT_KEY );

      // si todo sale bien seguimos sino lo agarra el catch
      // agregamops el uid en el request para ser usado en toda la aplicacion
      req.uid = uid;

      // llamamos el call back en caso que todo bien
      next();
     
   } catch (error) {
      console.log('JWT no valido');
      return res.status(401).json({
         ok: false,
         msg: 'Token no valido'
      }); 
   } 

}

module.exports = {
   validar_JWT
}